# StockLinker #


This extension provides the feature of connect the stock of two Magento e-commerce webpages

## Features ##

* Works with Magento from version 1.4.1.1 to 1.9.1.1
* It connects both websites through GET and POST queries
* Both websites share a encrypted handshake password to improve security
* You can also connect through a proxy
* You can set a Simple HTTP Auth to verify the connection

## How do I get set up? ##

### Installation ###
1. Disable Cache from ``` System > Cache Managment```
2. Disable Compilation from ``` System > Tools > Compilation ```
3. Copy all the files to the root of your Magento installation using FTP or other method
4. Flush ``` Magento Cache ```and ``` Cache Storage ``` from ``` System > Cache Managment ```
5. Log out from the admin backend, and log in again.
6. You can enable now Compilation and Cache.

Note: Both websites must have the extension installed.

**IMPORTANT**: Both websites MUST have the same SKU on the products they want to synchronize.

### Configuration ###
1. Go to the backend, click the 'System' tab, and at the menu, you will see 'StockLinker'
2. At ``` External Website URL ``` write the website you want to connect
3. Choose the synchronization method (GET or POST). Note: Both websites MUST have the same sync method.
4. Write a complex handshake password
5. [Optional] If you have a Simple HTTP authentication, write here your credentials with the format ``` username:password ```
6. [Optional] If you want to connect through a proxy, write here the proxy's IP in the format ``` ipaddress:port ```

### How to run tests ###
You can edit the quantity at the inventory tab of a product, and check if it changes as well on the other website.

### Deployment instructions ###
If you want to edit some code, you will find it at ``` app/code/local/Fede/StockLinker/ ```

### Contribution ###
You can fork this repository, make the changes you want and create a [Pull Request](https://gitlab.com/tembleking/stocklinker/merge_requests/new).

If you find some bug, or you have any issue, don't hesitate to write an [Issue Post](https://gitlab.com/tembleking/stocklinker/issues/new) (please, don't duplicate issues)

## License ##
Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.