<?php
    /*
        Fede_StockLinker Magento's Extension
        [https://gitlab.com/tembleking/stocklinker/]
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_StockLinker_IndexController extends Mage_Core_Controller_Front_Action
    {
        public function indexAction()
        {
            // Helper has log() and cast() functions
            $_helper = Mage::helper('fede_stocklinker');
            
            // Handle GET query
            if(Mage::getStoreConfig('fede_stocklinker/general/sync_method') == 0 && isset($_GET['sku']) && isset($_GET['qty']) && isset($_GET['hs']))
            {
                $this->triggerProductSync(urldecode($_GET['sku']), urldecode($_GET['qty']), urldecode($_GET['hs']));
            }
            // Handle POST query
            elseif(Mage::getStoreConfig('fede_stocklinker/general/sync_method') == 1 && isset($_POST['sku']) && isset($_POST['qty']) && isset($_POST['hs']))
            {
                $this->triggerProductSync(urldecode($_POST['sku']), urldecode($_POST['qty']), urldecode($_POST['hs']));
            }
            // Not a correct GET or POST? 404!
            else
            {
                header('HTTP/1.0 404 Not Found');
                echo '<h1>404 Not Found</h1>';
                echo 'The page that you have requested could not be found.<br />';
                exit();
            }
        }
        
        private function triggerProductSync($sku, $qty, $hs)
        {
            $_helper = Mage::helper('fede_stocklinker');
            
            if(md5(Mage::getStoreConfig('fede_stocklinker/general/handshake')) !== $hs)
            {
                $_helper->log('Handshake provided doesn\'t match. A possible attack has been blocked.');
                return;
            }
            
            $product = Mage::getSingleton('catalog/product')->loadByAttribute('sku', $sku);
            
            if(!$product)
            {
                $_helper->log('This product could not be loaded, maybe the SKU is not valid.');
                return;
            }
            
            $stock = Mage::getSingleton('cataloginventory/stock_item')->loadByProduct($product);
            
            if(!$stock)
            {
                $_helper->log('The stock info of this product could not be loaded.');
                return;
            }
            
            $_helper->cast('Fede_StockLinker_Model_Item', $stock)->setData('qty', $qty)->save();
            $_helper->log('Stock of %s modified to %s', $sku, $qty);
        }
    }
?>