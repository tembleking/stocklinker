<?php
    /*
        Fede_StockLinker Magento's Extension
        [https://gitlab.com/tembleking/stocklinker/]
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_StockLinker_IndexController extends Mage_Core_Controller_Front_Action
    {
        public function indexAction()
        {
            $_helper = Mage::helper('fede_stocklinker');
            if(!$_helper->isExtensionEnabled())
                return;
            
            // Handle GET query
            if($this->isInformationCorrectGet())
            {
                $this->triggerProductSync($_GET['sku'], $_GET['qty'], $_GET['hs']);
            }
            // Handle POST query
            elseif($this->isInformationCorrectPost())
            {
                $this->triggerProductSync($_POST['sku'], $_POST['qty'], $_POST['hs']);
            }
            // Not a correct GET or POST? 404!
            else
            {
                $this->send404Error();
            }
        }
        
        private function triggerProductSync($sku, $qty, $hs)
        {
            $sku = urldecode($sku);
            $qty = urldecode($qty);
            $hs  = urldecode($hs);
        
            $_helper = Mage::helper('fede_stocklinker');
            
            if(!$this->isHandshakeCorrect($hs))
            {
                $_helper->log('Handshake provided doesn\'t match. A possible attack has been blocked.');
                return;
            }
            
            $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
            if(!$id)
            {
                $_helper->log('Could not find the Product ID for the SKU: %d. Maybe the SKU provided is not valid, or it doesn\'t exist in the database.', $sku);
                return;
            }
            
            $product = Mage::getModel('catalog/product')->load($id);
            if(!$product)
            {
                $_helper->log('This product could not be loaded, maybe the ID (%d) is not valid.', $id);
                return;
            }
            
            $stock = Mage::getSingleton('cataloginventory/stock_item')->loadByProduct($product);
            if(!$stock)
            {
                $_helper->log('The stock information of this product could not be loaded.');
                return;
            }
            
            // Casting the object to our custom class will avoid recursivity on event fired
            $safeStock = $_helper->cast('Fede_StockLinker_Model_Item', $stock);
            
            // Update the quantity of items on stock and save it on the database
            $safeStock->setData('qty', $qty)->save();
            
            $_helper->log('Stock of %s modified to %s', $sku, $qty);
        }
        
        private function isHandshakeCorrect($hs)
        {
            $_helper = Mage::helper('fede_stocklinker');
            return ($_helper->getHandshakeFromConfig() === $hs);
        }
        
        private function isInformationCorrectGet()
        {
            return (Mage::getStoreConfig('fede_stocklinker/general/sync_method') == 0 && isset($_GET['sku']) && isset($_GET['qty']) && isset($_GET['hs']));
        }
        
        private function isInformationCorrectPost()
        {
            return (Mage::getStoreConfig('fede_stocklinker/general/sync_method') == 1 && isset($_POST['sku']) && isset($_POST['qty']) && isset($_POST['hs']));
        }
        
        private function send404Error()
        {
            header('HTTP/1.0 404 Not Found');
            echo '<h1>404 Not Found</h1>';
            echo 'The page that you have requested could not be found.<br />';
            exit();
        }
        
    }
?>
