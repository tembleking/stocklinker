<?php
    /*
        Fede_StockLinker Magento's Extension
        [https://gitlab.com/tembleking/stocklinker/]
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_StockLinker_Model_Observer
    {
        public function __construct()
        {
            $this->_helper = Mage::helper('fede_stocklinker');
        }
        
        private function queryInventoryStockChange($sku, $qty)
        {
            if(Mage::getStoreConfig('fede_stocklinker/general/externalwebsite') !== '' && Mage::getStoreConfig('fede_stocklinker/general/handshake') !== '')
            {
                $curl = curl_init();
                
                // GET
                if(Mage::getStoreConfig('fede_stocklinker/general/sync_method') == 0)
                {
                    curl_setopt($curl, CURLOPT_URL, 
                        Mage::getStoreConfig('fede_stocklinker/general/externalwebsite') . 
                        '/index.php/stocklinker/?sku=' . urlencode($sku) . 
                        '&qty=' . urlencode($qty) . 
                        '&hs=' . urlencode(md5(Mage::getStoreConfig('fede_stocklinker/general/handshake')))
                    );
                }
                // POST
                else 
                {
                    curl_setopt($curl, CURLOPT_URL, Mage::getStoreConfig('fede_stocklinker/general/externalwebsite') . '/index.php/stocklinker/');
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                        sku => urlencode($sku), 
                        qty => urlencode($qty), 
                        hs => urlencode(md5(Mage::getStoreConfig('fede_stocklinker/general/handshake')))
                    ));
                }
                
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
                
                if(Mage::getStoreConfig('fede_stocklinker/general/simple_auth') !== '')
                {
                    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($curl, CURLOPT_USERPWD, Mage::getStoreConfig('fede_stocklinker/general/simple_auth'));
                }
                
                if(Mage::getStoreConfig('fede_stocklinker/general/proxy') !== '')
                {
                    curl_setopt($curl, CURLOPT_PROXY, Mage::getStoreConfig('fede_stocklinker/general/proxy'));
                }
                
                $result = curl_exec($curl);
                if($result != '')
                {
                    $this->_helper->log('Query output: %s', $result);
                }
                
                curl_close($curl);
            }
        }
        
        public function catalogInventorySave(Varien_Event_Observer $observer)
        {
            $event = $observer->getEvent();
            $_item = $event->getItem();
            $_product = Mage::getSingleton('catalog/product')->load($_item->getProductId());
            
            if ($_product !== null && ((int)$_item->getData('qty') != (int)$_item->getOrigData('qty'))) 
            {
                $params = array();
                $params['event'] = 'catalogInventorySave';
                $params['sku'] = $_product->getSku();
                $params['name'] = $_product->getName();
                $params['price'] = $_product->getPrice();
                $params['product_id'] = $_item->getProductId();
                $params['qty'] = $_item->getQty();
                $params['qty_change'] = $_item->getQty() - $_item->getOrigData('qty');
                
                $this->_helper->log(var_export($params, true));
                $this->queryInventoryStockChange($params['sku'], $params['qty']);
            }
        }
        
        public function subtractQuoteInventory(Varien_Event_Observer $observer)
        {
            $quote = $observer->getEvent()->getQuote();
            foreach ($quote->getAllItems() as $item) 
            {
                $params = array();
                $params['event'] = 'subtractQuoteInventory';
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $params['qty'] = ($item->getProduct()->getStockItem()->getQty()) - ($item->getTotalQty());
                $params['qty_change'] = ($item->getTotalQty() * -1);
                
                $this->_helper->log(var_export($params, true));
                $this->queryInventoryStockChange($params['sku'], $params['qty']);
            }
            
        }
        
        public function revertQuoteInventory(Varien_Event_Observer $observer)
        {
            $quote = $observer->getEvent()->getQuote();
            foreach ($quote->getAllItems() as $item) 
            {
                $params = array();
                $params['event'] = 'revertQuoteInventory';
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $params['qty'] = $item->getProduct()->getStockItem()->getQty();
                $params['qty_change'] = $item->getTotalQty();
                
                $this->_helper->log(var_export($params, true));
                $this->queryInventoryStockChange($params['sku'], $params['qty']);
            }
            
        }
        
        public function cancelOrderItem(Varien_Event_Observer $observer)
        {
            $item = $observer->getEvent()->getItem();
            $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();
            $params = array();
            $params['event'] = 'cancelOrderItem';
            $params['product_id'] = $item->getProductId();
            $params['sku'] = $item->getSku();
            $params['qty'] = $item->getProduct()->getStockItem()->getQty();
            $params['qty_change'] = $qty;
            
            $this->_helper->log(var_export($params, true));
            $this->queryInventoryStockChange($params['sku'], $params['qty']);
            
        }
        
        public function refundOrderInventory(Varien_Event_Observer $observer)
        {
            $creditmemo = $observer->getEvent()->getCreditmemo();
            foreach ($creditmemo->getAllItems() as $item) 
            {
                $params = array();
                $params['event'] = 'refundOrderInventory';
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $params['qty'] = $item->getProduct()->getStockItem()->getQty();
                $params['qty_change'] = $item->getQty();
                
                $this->_helper->log(var_export($params, true));
                $this->queryInventoryStockChange($params['sku'], $params['qty']);
            }
        }
    }    
?>