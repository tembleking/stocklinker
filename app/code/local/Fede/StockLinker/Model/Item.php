<?php
    /*
        Fede_StockLinker Magento's Extension
        [https://gitlab.com/tembleking/stocklinker/]
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    // This class will override the event throw, so we can avoid the recursivity.
    class Fede_StockLinker_Model_Item extends Mage_CatalogInventory_Model_Stock_Item
    {
        public function __construct()
        {
            parent::__construct();
        }
        
        protected $_eventPrefix = 'fede_stocklinker_model_item';
        
        
        protected function _afterSave()
        {
            return $this;
        }
    }
?>