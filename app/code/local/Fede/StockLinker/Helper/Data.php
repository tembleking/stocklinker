<?php 
    /*
        Fede_StockLinker Magento's Extension
        [https://gitlab.com/tembleking/stocklinker/]
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_Stocklinker_Helper_Data extends Mage_Core_Helper_Abstract
    {
        public function log()
        {
            $file = "fede_stocklinker.log";
            $numArgs = func_num_args();
            
            if($numArgs == 0)
                return;
            
            if($numArgs == 1)
            {
                Mage::log(var_export(func_get_arg(0), true), null, $file);
                return;
            }
            
            if($numArgs > 1)
            {
                $text = func_get_arg(0);
                $args = array();
                for($i = 1; $i < $numArgs; ++$i)
                    $args[] = func_get_arg($i);
                Mage::log(vsprintf(var_export($text, true), $args), null, $file);
                return;
            }
        }
        
        public function cast($class, $object)
        {
            return unserialize(preg_replace('/^O:\d+:"[^"]++"/', 'O:' . strlen($class) . ':"' . $class . '"', serialize($object)));
        }
        
        public function isExtensionEnabled()
        {
            return Mage::getStoreConfig('fede_stocklinker/general/isextensionenabled') == 1;
        }
        
        public function getSimpleHTTPAuthFromConfig()
        {
            if(Mage::getStoreConfig('fede_stocklinker/http_auth/simple_auth_user') == '' || Mage::getStoreConfig('fede_stocklinker/http_auth/simple_auth_pass') == '')
                return '';
            
            return Mage::getStoreConfig('fede_stocklinker/http_auth/simple_auth_user') . ':' . Mage::getStoreConfig('fede_stocklinker/http_auth/simple_auth_pass');
        }
        
        public function getProxyFromConfig()
        {
            if(Mage::getStoreConfig('fede_stocklinker/proxy/proxy_ip') == '' || Mage::getStoreConfig('fede_stocklinker/proxy/proxy_port') == '')
                return '';
            
            return Mage::getStoreConfig('fede_stocklinker/proxy/proxy_ip') . ':' . Mage::getStoreConfig('fede_stocklinker/proxy/proxy_port');
        }
        
        /**
            * Iterative salted password hash
            * We can't use password_hash() which is more secure, but it's only
            * compatible with PHP>=5.5.0
        */
        public function getHandshakeFromConfig()
        {
            $firstHandshake = Mage::getStoreConfig('fede_stocklinker/handshakes/handshake');
            $secondHandshake = Mage::getStoreConfig('fede_stocklinker/handshakes/handshake2');
            
            $secureHash = $this->pbkdf2('SHA256', $firstHandshake, $secondHandshake, 1000, 256, true);
            
            return $secureHash;
        }
        
        
        
        
        /*
            * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
            * $algorithm - The hash algorithm to use. Recommended: SHA256
            * $password - The password.
            * $salt - A salt that is unique to the password.
            * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
            * $key_length - The length of the derived key in bytes.
            * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
            * Returns: A $key_length-byte key derived from the password and salt.
            *
            * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
            *
            * This implementation of PBKDF2 was originally created by https://defuse.ca
            * With improvements by http://www.variations-of-shadow.com
            *
            * Source Code: https://github.com/defuse/password-hashing
        */
        private function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false)
        {
            $algorithm = strtolower($algorithm);
            if(!in_array($algorithm, hash_algos(), true))
                trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
            if($count <= 0 || $key_length <= 0)
                trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);
            if (function_exists("hash_pbkdf2")) {
                // The output length is in NIBBLES (4-bits) if $raw_output is false!
                if (!$raw_output) {
                    $key_length = $key_length * 2;
                }
                return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
            }
            $hash_length = strlen(hash($algorithm, "", true));
            $block_count = ceil($key_length / $hash_length);
            $output = "";
            for($i = 1; $i <= $block_count; $i++) {
                // $i encoded as 4 bytes, big endian.
                $last = $salt . pack("N", $i);
                // first iteration
                $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
                // perform the other $count - 1 iterations
                for ($j = 1; $j < $count; $j++) {
                    $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
                }
                $output .= $xorsum;
            }
            if($raw_output)
                return substr($output, 0, $key_length);
            else
                return bin2hex(substr($output, 0, $key_length));
        }
    }
?>